﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TestDemo.Data.Repository;
using TestDemo.Domain.DTO;
using TestDemo.Domain.Entitties;

namespace TestDemo.Presentation.Controllers
{
    public class TestDemoController : Controller
    {
        private readonly ILogger<TestDemoController> _logger;
        private readonly IGenericRepository _genericRepository;
    
        public TestDemoController(ILogger<TestDemoController> logger,
            IGenericRepository genericRepository)
        {
            _logger = logger;
            _genericRepository = genericRepository;        
        }

        public async Task<IActionResult> Index()
        {           
            return View();
        }

        [HttpGet("/get")]
        public async Task<IActionResult> Get()
        {
            var result = await _genericRepository.GetTenantsAsync();
            return Ok(result);
        }

        [HttpGet("/getByGender")]
        public async Task<IActionResult> Index(int id)
        {
            var result = await _genericRepository.GetByGenderAsync(id);
            return Ok(result);
        }

        [HttpDelete("/delete")]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _genericRepository.DeleteTenantAsync(id);
            return Ok(result);
        }

        [HttpPost("/create")]
        public async Task<IActionResult> Create([FromBody] TenantPersonnelDTO model)
        {
            var result = await _genericRepository.CreateTenantAsync(model);
            return Ok(result);   
        }

        [HttpPut("/update")]
        public async Task<IActionResult> Update([FromBody]EditTenantDTO data)
        {
            var result = await _genericRepository.EditTenantAsync(data);
            return Ok(result);
        }
    }
}
