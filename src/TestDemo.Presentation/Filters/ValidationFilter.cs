﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestDemo.Domain.DTO;

namespace TestDemo.Presentation.Filters
{
    public class ValidationFilter : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var state = context.ModelState;

            if (!state.IsValid)
            {
                context.HttpContext.Response.StatusCode = 400;
                var errors = state.Values.Where(v => v.Errors.Count > 0)
                        .SelectMany(v => v.Errors)
                        .Select(v => v.ErrorMessage)
                        .ToList();

               await context.HttpContext.Response.WriteAsync(String.Join(" ; ", errors));
               return;
            }
           
            await next();
        }
    }
}
