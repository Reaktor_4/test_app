﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestDemo.Domain.DTO;
using TestDemo.Domain.Entitties;

namespace TestDemo.Presentation.Mappers
{
    public class TenantPersonnelProfile : Profile
    {
        public TenantPersonnelProfile()
        {
            CreateMap<TenantPersonnelDTO, TenantPersonnel>()
                .ForMember(x => x.GenderFk, opt => opt.Ignore());


            CreateMap<TenantPersonnel, TenantPersonnelDTO>()
             .ForMember(x => x.GenderId, opt => opt.MapFrom(m => m.GenderFk.Id));
             
            CreateMap<EditTenantDTO, TenantPersonnel>()
              .ForMember(x => x.GenderFk, opt => opt.Ignore())
              .ForMember(x => x.Id, opt => opt.Ignore())
              .ReverseMap();
        }
    }
}
