﻿$(document).ready(function () {    
    get();
    tBody = $('#userList');
});

let tenantsList = [];
let tBody;
let editMode;
let validator;
let selectedId;
const baseEndpoint = 'https://localhost:44363'

$("#gender-filter").change(function () {
    const genderId = parseInt($("#gender-filter").val());

    if (genderId)
        getById(genderId);
    else
        get();
});

//requests
function getById(id) {
    $.ajax({
        url: baseEndpoint + '/getByGender',
        data: {id},
        dataType: 'json'
    })
        .then(
            (data) => data.isSuccess ? buildTable(data.data) : handleError(data.errorMessage),
            (error) => handleError(error.errorMessage)
        )
};

function get() {
    $.ajax({
        url: baseEndpoint + '/get',
        dataType: 'json'
    })
        .then(
            (data) => data.isSuccess ? buildTable(data.data) : handleError(data.errorMessage),
            (error) => handleError(error.errorMessage)
        )
};

function remove(id) {
    $.ajax({
        url: baseEndpoint + '/delete',
        data: { id },
        method: 'DELETE',
        dataType: 'json'
    })
        .then(
            (data) => data.isSuccess ? removeData(id) : handleError(data.errorMessage),
            (error) => handleError(error)
        )
};

function create(model) {
    $.ajax({
        url: baseEndpoint + '/create',
        async: true,
        type: 'POST',
        data: JSON.stringify(model),
        headers: {
            'Content-Type': 'application/json'
        },
        dataType: 'json',
    })
        .then(
            (data) => data.isSuccess ? insertData(data.data) : handleError(data.errorMessage),
            (error) => handleError(error)
        )
}

function edit(model) {    
    $.ajax({
        url: baseEndpoint + '/update',
        method: 'PUT',
        data: JSON.stringify(model),
        headers: {
            'Content-Type': 'application/json'
        },
        dataType: 'json',
    })
        .then(
            (data) => data.isSuccess ? updateData(data.data) : handleError(data.errorMessage),
            (error) => handleError(error)
        ) 
}

//response handling
function buildTable(data) {
    let trHTML = '';
    $.each(data, function (i, item) {
        trHTML += buildRow(i, item);      
    });

    if (tenantsList.length) {
        $.each(tenantsList, (i, item) => $('#' + item.id).remove());
        tenantsList = [];
    }

    tBody.append(trHTML);
    tenantsList = data;
}


function insertData(data) {
    let htmlRow = buildRow(tenantsList.length, data);
    tBody.append(htmlRow);
    tenantsList.push(data);
    $("#tenant-modal").modal('toggle');
}


function removeData(id) {
    $("#" + id).remove();
    const removedElement = tenantsList.find(f => f.id === id);
    const index = tenantsList.indexOf(removedElement);

    if (index !== -1)
        tenantsList.splice(index, 1);
}

function updateData(model) {
    var selectedRaw = $('#' + model.existedId);
    selectedRaw.find("td").eq(1).html(`${model.firstName} ${model.middleName}  ${model.lastName}`);
    selectedRaw.find("td").eq(2).html(model.nickName);
    selectedRaw.find("td").eq(3).html(model.isActive || 'false');
    var selectedItem = tenantsList.find(f => f.id === selectedId);
    var index = tenantsList.indexOf(selectedItem);
    tenantsList[index] = model;
    tenantsList[index].id = model.existedId;
    $("#tenant-modal").modal('toggle');
}

function handleError(error) {
    alert(error);
}

//Modal area

function deleteOpenModal(id) {
    $.confirm({
        title: 'Confirm!',
        content: 'Simple confirm!',
        buttons: {
            confirm: function () {
                remove(id);
            },
            cancel: function () {
               
            },
        }
    });
}

function setupValidation() {
    $("#tenant-form").validate({          
        rules: {
            firstname:
            {
                required: true,
                maxlength: 50
            },
            lastname:
            {
                required: true,
                maxlength: 50
            },     
             middlename:
            {               
                 maxlength: 50
            },
            nickname:
            {                
                maxlength: 25
            }
        },
        messages: {
            firstname: "Please enter a valid first name.",
            middlename: "Please enter a valid middle name.",
            lastname: "Please enter your last name.",  
            nickname: "Please enter a valid nickname."
        },

    });  
}

function openModal(id) {
    $("#tenant-modal").modal();
    editMode = id ? true : false;
   
    if (editMode) {
        selectedId = id;
        let selectedtenant = tenantsList.find(f => f.id === id);
        $('#firstname').val($('#firstname').val() + selectedtenant.firstName);
        $('#middlename').val($('#middlename').val() + selectedtenant.middleName);
        $('#lastname').val($('#lastname').val() + selectedtenant.lastName);
        $('#nickname').val($('#nickname').val() + selectedtenant.nickName);
        $("#isactive").prop("checked", selectedtenant.isActive);
        $(`#gender option[value=${selectedtenant.genderId}]`).attr("selected", true);
    } 
}


function submitForm() {    
    if ($("#tenant-form").valid()) {
        const data = {
            firstName: $('#firstname').val(),
            lastName: $('#lastname').val(),
            middleName: $('#middlename').val(),
            nickName: $('#nickname').val(),
            isActive: $("#isactive").prop("checked"),
            genderId: parseInt($("#gender").val())
        }

        if (editMode) {
            data.existedId = selectedId;
            edit(data);
            return;
        }

        create(data)
    }
}

$('#tenant-modal').on('hidden.bs.modal', function () {
    $('#firstname').val('');
    $('#middlename').val('');
    $('#lastname').val('');
    $('#nickname').val('');
    $("#isactive").prop("checked", false);
})

$("#tenant-modal").on('shown.bs.modal', function () {
    setupValidation();
});

$('#tenant-form').on('submit', function (e) {
    e.preventDefault();
    e.stopPropagation();
});

//Other
function generateButtonsBlockHtml(item) {
    let buttonBlockHtml =
        `<button onclick="deleteOpenModal(${item.id})" class="btn btn-danger btn-action"> Remove </button>`
        + `<button onclick="openModal(${item.id})" class="btn btn-info btn-action"> Edit </button>`


    return buttonBlockHtml;
}

function buildRow(i, item) {
    let gender = item.genderId === 1 ? "Male" : "Female";
    return `<tr id = ${item.id}>`
        + `<td>${i}</td> <td>${item.firstName} ${item.middleName || ''} ${item.lastName} </td> <td>`
        + item.nickName + '</td><td>'
        + item.isActive + '</td><td>'
        + gender + '</td><td>'
        + generateButtonsBlockHtml(item) + '</td> </tr>'
}