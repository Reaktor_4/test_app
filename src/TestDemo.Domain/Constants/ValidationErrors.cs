﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestDemo.Domain.Constants
{
    public static class ValidationErrors
    {
        public const string IdIsEmpty = "The item was not selected";
        public const string FirstNameStringRange = "The first name should be no more than 50 symbols and no less 1 symbol";
        public const string FirstNameRequired = "The first name is required";
        public const string MiddleNameStringRange = "The middle name should be no more than 50 symbols and no less 1 symbol";
        public const string LastNameRequired = "The last name is required";
        public const string LastNameStringRange = "The last name should be no more than 50 symbols and no less 1 symbol";
        public const string NickNameStringRange = "The nickname is required";
        public const string GenderStringRange = "The gender should be no more than 25 symbols and no less 1 symbol";
        public const string GenderRequired = "The gender is required";
    }
}
