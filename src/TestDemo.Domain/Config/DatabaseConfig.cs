﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestDemo.Domain.Config
{
    public class DatabaseConfig
    {
        public string ConnectionString { get; set; }
    }
}
