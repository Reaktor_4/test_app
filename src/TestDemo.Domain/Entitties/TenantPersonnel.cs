﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TestDemo.Domain.Constants;

namespace TestDemo.Domain.Entitties
{
    [Table("TenantPersonnel")]

    public class TenantPersonnel : BaseEntity
    {
        public int? TenantId { get; set; }

        [Required (ErrorMessage = ValidationErrors.FirstNameRequired)]
        [StringLength(maximumLength: 50, MinimumLength = 1, ErrorMessage = ValidationErrors.FirstNameStringRange)]
        public virtual string FirstName { get; set; }
                
        [StringLength(maximumLength: 50,  ErrorMessage = ValidationErrors.MiddleNameStringRange)]
        public virtual string MiddleName { get; set; }

        [Required(ErrorMessage =ValidationErrors.LastNameRequired)]
        [StringLength(maximumLength: 50, MinimumLength = 1, ErrorMessage = ValidationErrors.LastNameStringRange)]
        public virtual string LastName { get; set; }

        [StringLength(maximumLength: 25, ErrorMessage = ValidationErrors.NickNameStringRange)]
        public virtual string NickName { get; set; }
        public virtual DateTime DOB { get; set; }
        public virtual int PrefixId { get; set; }

        [ForeignKey("GenderId")]
        public Gender GenderFk { get; set; }
    }
}
