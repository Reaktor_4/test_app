﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TestDemo.Domain.Entitties
{
   public class BaseEntity
    {
        [Key]
        public int Id { get; set; }

        public bool IsActive { get; set; } = true;

        public DateTime Created => DateTime.UtcNow;

        public DateTime? LastUpdating { get; set; }
    }
}
