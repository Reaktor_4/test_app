﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TestDemo.Domain.Constants;

namespace TestDemo.Domain.Entitties
{
    [Table("Genders")]
    public class Gender : BaseEntity
    {
        [Required (ErrorMessage = ValidationErrors.GenderRequired)]
        [StringLength(maximumLength: 25, MinimumLength = 1, ErrorMessage = ValidationErrors.GenderStringRange)]
        public virtual string Name { get; set; }

    }
}
