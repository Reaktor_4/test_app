﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TestDemo.Domain.Constants;

namespace TestDemo.Domain.DTO
{
    public class TenantPersonnelDTO
    {
        public int Id { get; set; }

        [Required(ErrorMessage = ValidationErrors.FirstNameRequired)]
        [StringLength(maximumLength: 50, MinimumLength = 1, ErrorMessage = ValidationErrors.FirstNameStringRange)]
        public string FirstName { get; set; }

        [StringLength(maximumLength: 50, ErrorMessage = ValidationErrors.MiddleNameStringRange)]
        public string MiddleName { get; set; }

        [Required(ErrorMessage = ValidationErrors.LastNameRequired)]
        [StringLength(maximumLength: 50, MinimumLength = 1, ErrorMessage = ValidationErrors.LastNameStringRange)]
        public string LastName { get; set; }

        [Required(ErrorMessage = ValidationErrors.GenderRequired)]        
        public int GenderId { get; set; }

        [StringLength(maximumLength: 25, ErrorMessage = ValidationErrors.NickNameStringRange)]
        public string NickName { get; set; }

        public bool IsActive { get; set; } = true;

    }
}
