﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace TestDemo.Domain.DTO
{
    public class BaseResponseDTO
    {
        public bool IsSuccess { get; set; } = true;


        public HttpStatusCode StatusCode { get; set; }

        public string ErrorMessage { get; set; }

        public BaseResponseDTO(HttpStatusCode code = HttpStatusCode.OK)
        {
            StatusCode = code;
        }

        public BaseResponseDTO(HttpStatusCode code, string errorMessage)
        {
            IsSuccess = false;
            StatusCode = code;
            ErrorMessage = errorMessage;
        }
    }
}
