﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TestDemo.Domain.Constants;

namespace TestDemo.Domain.DTO
{
   public class EditTenantDTO : TenantPersonnelDTO
    {

        [Required(ErrorMessage = ValidationErrors.IdIsEmpty)]
        public int? ExistedId { get; set; }
    }
}
