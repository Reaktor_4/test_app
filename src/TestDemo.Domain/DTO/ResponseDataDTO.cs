﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace TestDemo.Domain.DTO
{
    public class ResponseDataDTO<T> : BaseResponseDTO
    {
        public T Data { get; set; }

        public ResponseDataDTO(HttpStatusCode statusCode, string errorMessage)
            : base(statusCode, errorMessage) { }

        public ResponseDataDTO(T data, HttpStatusCode code = HttpStatusCode.OK) : base(code)
        {
            Data = data;
        }
    }
}
