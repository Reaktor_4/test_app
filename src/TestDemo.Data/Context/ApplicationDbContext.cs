﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestDemo.Domain.Entitties;

namespace TestDemo.Data.Context
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
       : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Gender>().HasData(new Gender() { Id = 1, Name = "Male" }, new Gender() { Id = 2, Name = "Female" });

            base.OnModelCreating(builder);
        }

        public DbSet<Gender> Genders { get; set; }
        public DbSet<TenantPersonnel> TenantPersonnels { get; set; }

    }
}
