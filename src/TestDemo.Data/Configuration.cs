﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using TestDemo.Data.Context;
using TestDemo.Data.Repository;
using TestDemo.Domain.Config;

namespace TestDemo.Data
{
    public static class Configuration
    {
        public static IServiceCollection ConfigureDataAccess(this IServiceCollection services, IConfiguration configuration)
        {
            try
            {
                var connection = configuration.GetSection("Db").Get<DatabaseConfig>();
                services.AddDbContext<ApplicationDbContext>(
                         options => options.UseSqlServer(connection.ConnectionString));

                services.AddTransient<IGenericRepository, GenericRepository>();          
            }
            catch (Exception ex)
            {
                //to do handling error
            }

            return services;
        }

    }
}
