﻿using Microsoft.EntityFrameworkCore;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestDemo.Data.Context;
using TestDemo.Domain.Constants;
using TestDemo.Domain.DTO;
using TestDemo.Domain.Entitties;
using AutoMapper;
using System;

namespace TestDemo.Data.Repository
{
    public class GenericRepository : IGenericRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GenericRepository(ApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ResponseDataDTO<TenantPersonnelDTO>> CreateTenantAsync(TenantPersonnelDTO model)
        {
            var entityModel = _mapper.Map<TenantPersonnel>(model);
            entityModel.GenderFk = await _context.Genders.FirstOrDefaultAsync(f=>f.Id == model.GenderId);
            await _context.TenantPersonnels.AddAsync(entityModel);
            await _context.SaveChangesAsync();
            model.Id = entityModel.Id;

            return new ResponseDataDTO<TenantPersonnelDTO>(model, HttpStatusCode.Created); 
        }

        public async Task<BaseResponseDTO> DeleteTenantAsync(int id)
        {
            var data = await _context.TenantPersonnels.SingleOrDefaultAsync(f=>f.Id == id);

            if (data == null)
            {
                return new ResponseDataDTO<EditTenantDTO>(HttpStatusCode.NotFound, ResponseErrors.TenantsNotFound);
            }

            _context.Remove(data);
            await _context.SaveChangesAsync();

            return new BaseResponseDTO();
        }

        public async Task<ResponseDataDTO<EditTenantDTO>> EditTenantAsync(EditTenantDTO model)
        {
            var entityModel = await _context.TenantPersonnels.Include(x=>x.GenderFk).SingleOrDefaultAsync(x=>x.Id == model.ExistedId);

            if (entityModel == null)
            {
                return new ResponseDataDTO<EditTenantDTO>(HttpStatusCode.NotFound, ResponseErrors.TenantsNotFound);
            }

            entityModel = _mapper.Map(model, entityModel);
            entityModel.LastUpdating = DateTime.UtcNow;
            if (model.GenderId != entityModel.GenderFk.Id)
            {
                entityModel.GenderFk = await _context.Genders.FirstOrDefaultAsync(f=>f.Id == model.GenderId);
            }

           _context.TenantPersonnels.Update(entityModel);

            await _context.SaveChangesAsync();

            return new ResponseDataDTO<EditTenantDTO>(model);
        }

        public async Task<ResponseDataDTO<IEnumerable<TenantPersonnelDTO>>> GetByGenderAsync(int genderId)
        {
            var query = from person in _context.Set<TenantPersonnel>()                       
                        join gender in _context.Set<Gender>()
                            on person.GenderFk.Id equals gender.Id
                        where (gender.Id == genderId)
                        select person;

            var result = await query.Include(x=>x.GenderFk).Select(s=>_mapper.Map<TenantPersonnelDTO>(s)).ToListAsync();

            if (result.Any())
            {
                return new ResponseDataDTO<IEnumerable<TenantPersonnelDTO>>(result);
            }

            return new ResponseDataDTO<IEnumerable<TenantPersonnelDTO>>(HttpStatusCode.NotFound, ResponseErrors.TenantsNotFound);
        }

        public async Task<ResponseDataDTO<IEnumerable<TenantPersonnelDTO>>> GetTenantsAsync()
        {
            var tenants = await _context.TenantPersonnels.Include(x=>x.GenderFk).ToListAsync();
            if (tenants.Any())
            {
                return new ResponseDataDTO<IEnumerable<TenantPersonnelDTO>>(tenants.Select(s => _mapper.Map<TenantPersonnelDTO>(s)));
            }

            return new ResponseDataDTO<IEnumerable<TenantPersonnelDTO>>(HttpStatusCode.NotFound, ResponseErrors.TenantsNotFound);
        }

    }
}
