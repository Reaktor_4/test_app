﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TestDemo.Domain.DTO;
using TestDemo.Domain.Entitties;

namespace TestDemo.Data.Repository
{
    public interface IGenericRepository
    {
        Task<ResponseDataDTO<IEnumerable<TenantPersonnelDTO>>> GetTenantsAsync();

        Task<ResponseDataDTO<IEnumerable<TenantPersonnelDTO>>> GetByGenderAsync(int gender);

        Task<BaseResponseDTO> DeleteTenantAsync(int id);

        Task<ResponseDataDTO<EditTenantDTO>> EditTenantAsync(EditTenantDTO model);

        Task<ResponseDataDTO<TenantPersonnelDTO>> CreateTenantAsync(TenantPersonnelDTO model);
    }
}
