﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TestDemo.Data.Migrations
{
    public partial class TestDemoAstar_2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Genders",
                columns: new[] { "Id", "IsActive", "LastUpdating", "Name" },
                values: new object[] { 1, true, null, "Male" });

            migrationBuilder.InsertData(
                table: "Genders",
                columns: new[] { "Id", "IsActive", "LastUpdating", "Name" },
                values: new object[] { 2, true, null, "Female" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Genders",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Genders",
                keyColumn: "Id",
                keyValue: 2);
        }
    }
}
